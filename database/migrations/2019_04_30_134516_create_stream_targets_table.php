<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStreamTargetsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stream_targets', function (Blueprint $table) {
            $table->increments('id');
            $table->text('rtmp_target');
            $table->dateTime('start')->nullable();
            $table->dateTime('end')->nullable();
            $table->string('ffmpeg_args_0')->nullable();
            $table->string('ffmpeg_args_1')->nullable();
            $table->text('options')->nullable();
            $table->unsignedInteger('program_id')->nullable();
            $table->unsignedInteger('ad_id');
            $table->unsignedInteger('media_stream_id')->nullable();
            $table->foreign('program_id')->references('id')->on('programs');
            $table->foreign('ad_id')->references('id')->on('ads');
            $table->foreign('media_stream_id')->references('id')->on('media_streams');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('stream_targets');
    }
}
