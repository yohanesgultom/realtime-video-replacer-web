<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTvsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tvs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('rtmp_source');
            $table->string('logo_mask')->nullable();
            $table->string('logo_masked')->nullable();
            $table->unsignedInteger('logo_x')->nullable()->default(0);
            $table->unsignedInteger('logo_y')->nullable()->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tvs');
    }
}
