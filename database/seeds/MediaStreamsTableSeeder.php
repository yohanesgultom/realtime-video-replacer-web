<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use App\TV;
use App\MediaStream;

class MediaStreamsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tvs = TV::select('id')->get();
        foreach ($tvs as $tv) {
            MediaStream::insert([
                ['tv_id' => $tv->id, 'name' => 'Facebook', 'created_at' => Carbon::now()->toDateTimeString()],
                ['tv_id' => $tv->id, 'name' => 'Youtube', 'created_at' => Carbon::now()->toDateTimeString()],
            ]);
        }
    }
}
