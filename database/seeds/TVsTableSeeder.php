<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;
use Carbon\Carbon;
use App\TV;
use App\Program;

class TVsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $rtmp_source = 'rtmp://188.166.238.78:1938/sportfixasia/stream_01_sg';
        $logo_mask = new File('files/mask_1080.png');
        $logo_masked = new File('files/mask_result_1080.png');

        TV::insert([
            [
                'name' => 'TV One',
                'rtmp_source' => $rtmp_source,
                'logo_mask' => Storage::disk('local')->putFileAs("stream_targets/logo_mask", $logo_mask, $logo_mask->hashName()),
                'logo_masked' => Storage::disk('local')->putFileAs("stream_targets/logo_masked", $logo_masked, $logo_masked->hashName()),
                'logo_x' => 1436,
                'logo_y' => 98,
                'created_at' => Carbon::now()
            ],
            [
                'name' => 'TV Two',
                'rtmp_source' => $rtmp_source,
                'logo_mask' => Storage::disk('local')->putFileAs("stream_targets/logo_mask", $logo_mask, $logo_mask->hashName()),
                'logo_masked' => Storage::disk('local')->putFileAs("stream_targets/logo_masked", $logo_masked, $logo_masked->hashName()),
                'logo_x' => 1436,
                'logo_y' => 98,
                'created_at' => Carbon::now()
            ],
        ]);
        
        $programs = [];
        $program_count = 3;
        foreach (TV::select('id')->get() as $tv) {
            for ($i = 0; $i < $program_count; $i++) {
                $no = $i + 1;
                $programs[] = [
                    'name' => "Program {$no}",
                    'tv_id' => $tv->id,                
                    'created_at' => Carbon::now(),
                ];    
            }
        }
        Program::insert($programs);
    }
}
