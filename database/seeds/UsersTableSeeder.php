<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\TV;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tv = TV::select('id')->first();
        User::insert([
            [
                'name' => 'admin',
                'email' => 'admin@local.host',
                'role' => User::ROLES['ADMIN'],
                'tv_id' => null,
                'password' => Hash::make('admin'),
                'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
            ],
            [
                'name' => 'tvadmin',
                'email' => 'tvadmin@local.host',
                'role' => User::ROLES['TV_ADMIN'],
                'tv_id' => $tv->id,
                'password' => Hash::make('tvadmin'),
                'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
            ],
            [
                'name' => 'tvuser',
                'email' => 'tvuser@local.host',
                'role' => User::ROLES['TV_USER'],
                'tv_id' => $tv->id,
                'password' => Hash::make('tvuser'),
                'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
            ],
        ]);
    }
}
