<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;
use Carbon\Carbon;
use App\TV;
use App\Ad;

class AdsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {        
        $tvs = TV::select('id')->get();
        $files = [
            new File('files/video1.mp4'),
            // new File('files/video2.mp4'),
        ];

        foreach ($tvs as $tv) {
            Ad::insert([
                ['tv_id' => $tv->id, 'name' => 'Ad 1', 'created_at' => Carbon::now(), 'file' => Storage::disk('local')->putFileAs('ads/file', $files[0], $files[0]->hashName())],
                // ['name' => 'Ad 2', 'created_at' => Carbon::now(), 'file' => Storage::disk('local')->putFileAs('ads/file', $files[1], $files[1]->hashName())],
            ]);
        }
    }
}
