<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use App\TV;

class StreamTargetsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $rtmp_targets = [
            // 'Facebook' => '"rtmp://rtmp-pc.facebook.com:80/rtmp/2434440816608787?s_l=1&s_ps=1&s_sw=0&s_vt=api-pc&a=AbwgF6gp7FW2-SzP"',
            // 'Youtube' => 'rtmp://x.rtmp.youtube.com:1935/live2/g1jy-gq4p-7m9d-0a9p',
            'Facebook' => 'rtmp://x.rtmp.youtube.com/live2/4bgs-jgkk-mwkb-7zh3',
            'Youtube' => 'rtmp://x.rtmp.youtube.com/live2/4bgs-jgkk-mwkb-7zh3',
        ];
        $ffmpeg_args = [
            [
                'Facebook' => '-re -stream_loop -1 -i {ad_file} -vcodec libx264 -acodec copy -preset superfast -vb 1200k -maxrate 1200k -r 30 -bufsize 8000k -acodec aac -ac 1 -ar 44100 -b:a 128k -f flv {rtmp_target}',
                'Youtube' => '-re -i {ad_file} -c:v copy -c:a aac -f flv {rtmp_target}',
            ],
            [
                'Facebook' => '-re -i {rtmp_source} -c:v copy -c:a aac -r 44100 -b:a 128k -f flv {rtmp_target}',
                'Youtube' => '-re -i {rtmp_source} -c:v copy -c:a aac -f flv {rtmp_target}',
            ],
        ];        

        $tvs = TV::select('id')->get();
        foreach ($tvs as $tv) {
            $programs = DB::table('programs')->select('id')->where('tv_id', $tv->id)->get();
            $media_streams = DB::table('media_streams')->select('id', 'name')->where('tv_id', $tv->id)->get();
            $ads = DB::table('ads')->select('id')->where('tv_id', $tv->id)->get();

            $targets = [];
            $now = Carbon::now();
            $start = Carbon::now();
            $m = $media_streams->where('name', 'Youtube')->first();
            foreach ($programs as $p) {
                $start->addHours(1);                        
                $targets[] = [
                    'program_id' => $p->id,
                    'media_stream_id' => $m->id,
                    'ad_id' => $ads->random()->id,
                    'start' => $start->copy(),
                    'end' => $start->copy()->addMinutes(5),
                    'rtmp_target' => $rtmp_targets[$m->name],
                    'ffmpeg_args_0' => $ffmpeg_args[0][$m->name],
                    'ffmpeg_args_1' => $ffmpeg_args[1][$m->name],
                    'options' => json_encode(['cmd_overlap' => 0, 'threshold' => 0.1, 'consistency' => 2]),
                    'created_at' => $now,
                ];            
            }
            DB::table('stream_targets')->insert($targets);
        }
    }
}
