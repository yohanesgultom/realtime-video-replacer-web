<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::middleware(['auth'])->group(function () {
    Route::get('home', 'HomeController@index');
});

Route::prefix('admin')->name('admin.')->middleware(['auth', 'role:admin'])->group(function () {
    Route::get('tvs/{id}/download', 'Admin\TVController@download')->name('tvs.download');
    Route::resource('tvs', 'Admin\TVController');
});

Route::prefix('admin')->name('admin.')->middleware(['auth', 'role:admin,tv_admin'])->group(function () {
    Route::resource('users', 'Admin\UserController');
    Route::resource('programs', 'Admin\ProgramController');
});

Route::prefix('admin')->name('admin.')->middleware(['auth', 'role:admin,tv_admin,tv_user'])->group(function () {
    Route::get('ads/{id}/download', 'Admin\AdController@download')->name('ads.download');
    Route::resource('ads', 'Admin\AdController');
    Route::resource('mediaStreams', 'Admin\MediaStreamController');
    Route::resource('streamTargets', 'Admin\StreamTargetController');
});
