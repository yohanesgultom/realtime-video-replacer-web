<li class="header">MAIN</li>

<li class="{{ Request::is('admin/streamTargets*') ? 'active' : '' }}">
    <a href="{!! route('admin.streamTargets.index') !!}"><i class="fa fa-play"></i><span>Stream Target</span></a>
</li>

@if (Auth::user()->role_name == 'ADMIN')
<li class="{{ Request::is('admin/tvs*') ? 'active' : '' }}">
    <a href="{!! route('admin.tvs.index') !!}"><i class="fa fa-television"></i><span>TV</span></a>
</li>
@endif

@if (in_array(Auth::user()->role_name, ['ADMIN', 'TV_ADMIN']))
<li class="{{ Request::is('admin/programs*') ? 'active' : '' }}">
    <a href="{!! route('admin.programs.index') !!}"><i class="fa fa-film"></i><span>Program</span></a>
</li>
@endif

<li class="{{ Request::is('admin/ads*') ? 'active' : '' }}">
    <a href="{!! route('admin.ads.index') !!}"><i class="fa fa-video-camera"></i><span>Ads</span></a>
</li>

<li class="{{ Request::is('admin/mediaStreams*') ? 'active' : '' }}">
    <a href="{!! route('admin.mediaStreams.index') !!}"><i class="fa fa-rss"></i><span>Media Stream</span></a>
</li>

@if (in_array(Auth::user()->role_name, ['ADMIN', 'TV_ADMIN']))
<li class="header">SETTING</li>

<li class="{{ Request::is('admin/users*') ? 'active' : '' }}">
    <a href="{!! route('admin.users.index') !!}"><i class="fa fa-users"></i><span>User</span></a>
</li>
@endif
