<!-- RTMP Target Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('rtmp_target', 'RTMP Target:') !!}
    {!! Form::textarea('rtmp_target', null, ['class' => 'form-control', 'rows' => 2]) !!}
</div>

<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('ffmpeg_args_0', 'FFMPEG args 0:') !!}
    {!! Form::text('ffmpeg_args_0', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('ffmpeg_args_1', 'FFMPEG args 1:') !!}
    {!! Form::text('ffmpeg_args_1', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('options', 'Options:') !!}
    {!! Form::textarea('options', null, ['class' => 'form-control', 'rows' => 2]) !!}
</div>

<!-- Start Field -->
<div class="form-group col-sm-6">
    {!! Form::label('start', 'Start:') !!}
    {!! Form::text('start', null, ['class' => 'form-control datetimepicker']) !!}
</div>

<!-- End Field -->
<div class="form-group col-sm-6">
    {!! Form::label('end', 'End:') !!}
    {!! Form::text('end', null, ['class' => 'form-control datetimepicker']) !!}
</div>

<!-- Program Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('program_id', 'Program:') !!}
    {!! Form::select('program_id', \App\Program::all()->pluck('desc','id'), null, ['required' => true, 'placeholder' => 'Choose', 'class' => 'form-control']) !!}
</div>

<!-- Ad Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('ad_id', 'Ad:') !!}
    {!! Form::select('ad_id', \App\Ad::pluck('name','id'), null, ['required' => true, 'placeholder' => 'Choose', 'class' => 'form-control']) !!}
</div>

<!-- Media Stream Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('media_stream_id', 'Media Stream:') !!}
    {!! Form::select('media_stream_id', \App\MediaStream::pluck('name','id'), null, ['required' => true, 'placeholder' => 'Choose', 'class' => 'form-control']) !!}
</div>


<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('admin.streamTargets.index') !!}" class="btn btn-default">Cancel</a>
</div>

@section('scripts')
<script type="text/javascript">
$('.datetimepicker').datetimepicker({
    format: 'YYYY-MM-DD HH:mm:ss',
    useCurrent: false
})
</script>
@endsection
