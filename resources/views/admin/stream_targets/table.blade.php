<div class="table-responsive">
    <table class="table" id="streamTargets-table">
        <thead>
            <tr>
            <th>TV</th>
            <th>Program</th>
            <th>Ad</th>
            <th>Media Stream</th>
            <th>Start</th>
            <th>End</th>        
            <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($streamTargets as $streamTarget)
            <tr>
                <td>{!! $streamTarget->program->tv->name !!}</td>
                <td>{!! $streamTarget->program->name !!}</td>
                <td>{!! $streamTarget->ad->name !!}</td>
                <td>{!! $streamTarget->media_stream->name !!}</td>
                <td>{!! $streamTarget->start !!}</td>
                <td>{!! $streamTarget->end !!}</td>
                <td>
                    {!! Form::open(['route' => ['admin.streamTargets.destroy', $streamTarget->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('admin.streamTargets.edit', [$streamTarget->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>