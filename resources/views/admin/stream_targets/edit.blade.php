@extends('admin.layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Stream Target
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($streamTarget, ['route' => ['admin.streamTargets.update', $streamTarget->id], 'method' => 'patch']) !!}

                        @include('admin.stream_targets.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection