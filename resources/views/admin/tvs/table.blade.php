<div class="table-responsive">
    <table class="table" id="tVs-table">
        <thead>
            <tr>
                <th>Name</th>
                <th>Source</th>
                <th>Created At</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($tVs as $tV)
            <tr>
                <td>{!! $tV->name !!}</td>
                <td>{!! \Str::limit($tV->rtmp_source, 50) !!}</td>
                <td>{!! $tV->created_at !!}</td>
                <td>
                    {!! Form::open(['route' => ['admin.tvs.destroy', $tV->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('admin.tvs.edit', [$tV->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>