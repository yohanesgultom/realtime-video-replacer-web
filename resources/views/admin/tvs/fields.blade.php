<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['required' => true, 'class' => 'form-control']) !!}
</div>
<!-- RTMP Source Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('rtmp_source', 'RTMP Source:') !!}
    {!! Form::textarea('rtmp_source', null, ['class' => 'form-control', 'rows' => 2]) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('logo_mask', 'Logo mask:') !!}
    @if (!empty($tV) && !empty($tV->logo_mask))    
    <div>
        <img class="img-thumbnail" src="{{ route('admin.tvs.download', ['id' => $tV->id, 'file' => 'logo_mask']) }}">
    </div>
    @endif    
    {!! Form::file('logo_mask', ['class' => 'form-control']) !!}    
</div>

<div class="form-group col-sm-6">
    {!! Form::label('logo_masked', 'Logo masked:') !!}
    @if (!empty($tV) && !empty($tV->logo_masked))    
    <div>
        <img class="img-thumbnail" src="{{ route('admin.tvs.download', ['id' => $tV->id, 'file' => 'logo_masked']) }}">
    </div>
    @endif
    {!! Form::file('logo_masked', ['class' => 'form-control']) !!}      
</div>

<div class="form-group col-sm-6 col-lg-3">
    {!! Form::label('logo_x', 'Logo (top-left) X:') !!}
    {!! Form::number('logo_x', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6 col-lg-3">
    {!! Form::label('logo_y', 'Logo (top-left) Y:') !!}
    {!! Form::number('logo_y', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('admin.tvs.index') !!}" class="btn btn-default">Cancel</a>
</div>
