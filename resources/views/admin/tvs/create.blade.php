@extends('admin.layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            TV
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'admin.tvs.store', 'files' => true]) !!}

                        @include('admin.tvs.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
