<!-- Tv Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tv_id', 'TV:') !!}
    {!! Form::select('tv_id', \App\TV::pluck('name','id'), null, ['required' => true, 'placeholder' => 'Choose', 'class' => 'form-control']) !!}
</div>

<!-- Tv Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tv_id', 'TV:') !!}
    {!! Form::select('tv_id', \App\TV::pluck('name','id'), null, ['required' => true, 'placeholder' => 'Choose', 'class' => 'form-control']) !!}
</div>

<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['required' => true, 'class' => 'form-control']) !!}
</div>

<div class="clearfix"></div>

<!-- File Field -->
<div class="form-group col-sm-3">
    {!! Form::label('file', 'File:') !!}
    {!! Form::file('file', ['class' => 'form-control']) !!}
</div>

@if (!empty($ad))
<div class="clearfix"></div>
<div class="form-group col-sm-6">
    <video width="320" height="240" controls>
        <source src="{{ route('admin.ads.download', ['id' => $ad->id]) }}" type="video/mp4">
        Your browser does not support the video tag.
    </video>  
</div>
<div class="clearfix"></div>
@endif

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('admin.ads.index') !!}" class="btn btn-default">Cancel</a>
</div>
