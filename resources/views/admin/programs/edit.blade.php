@extends('admin.layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Program
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($program, ['route' => ['admin.programs.update', $program->id], 'method' => 'patch']) !!}

                        @include('admin.programs.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection