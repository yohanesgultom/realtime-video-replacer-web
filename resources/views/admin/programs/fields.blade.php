<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['required' => true, 'class' => 'form-control']) !!}
</div>

<!-- Tv Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tv_id', 'TV:') !!}
    {!! Form::select('tv_id', \App\TV::pluck('name','id'), null, ['required' => true, 'placeholder' => 'Choose', 'class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('admin.programs.index') !!}" class="btn btn-default">Cancel</a>
</div>
