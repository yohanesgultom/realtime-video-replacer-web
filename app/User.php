<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Builder;
use Auth;

class User extends Authenticatable
{
    use Notifiable;

    const ROLES = [
        'ADMIN' => 0,
        'TV_ADMIN' => 1,
        'TV_USER' => 2,
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 
        'role', 
        'email', 
        'password',
        'tv_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function tv()
    {
        return $this->belongsTo('App\TV');
    }
    
    public function getRoleNameAttribute()
    {
        return array_flip(static::ROLES)[$this->role];
    }

    public function scopeOwned($query)
    {
        if (Auth::check() && !empty(Auth::user()->tv_id)) {
            return $query->where('users.tv_id', Auth::user()->tv_id);
        }
        return $query;
    }

}
