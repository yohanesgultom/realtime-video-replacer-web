<?php

namespace App;

use Eloquent as Model;
use Auth;
use Illuminate\Database\Eloquent\Builder;

class Program extends Model
{

    public $table = 'programs';

    public $fillable = [
        'name',
        'tv_id'
    ];

    protected $with = [
        'tv',
    ];

    protected $append = [
        'desc',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'tv_id' => 'required'
    ];

    public function tv()
    {
        return $this->belongsTo('App\TV');
    }

    public function getDescAttribute()
    {
        return $this->tv->name.' - '.$this->name;
    }

    protected static function boot()
    {
        parent::boot();
        
        static::addGlobalScope('owned', function (Builder $builder) {
            if (Auth::check() && !empty(Auth::user()->tv_id)) {
                $builder->where('programs.tv_id', Auth::user()->tv_id);
            }
        });

        static::saving(function ($instance){
            if (Auth::check() && !empty(Auth::user()->tv_id)) {
                $instance->tv_id = Auth::user()->tv_id;
            }
        });
    }
}
