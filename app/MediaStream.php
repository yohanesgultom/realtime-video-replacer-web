<?php

namespace App;

use Eloquent as Model;
use Auth;
use Illuminate\Database\Eloquent\Builder;

class MediaStream extends Model
{

    public $table = 'media_streams';

    public $fillable = [
        'name',
        'tv_id',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required'
    ];

    protected static function boot()
    {
        parent::boot();
        
        static::addGlobalScope('owned', function (Builder $builder) {
            if (Auth::check() && !empty(Auth::user()->tv_id)) {
                $builder->where('media_streams.tv_id', Auth::user()->tv_id);
            }
        });

        static::saving(function ($instance){
            if (Auth::check() && !empty(Auth::user()->tv_id)) {
                $instance->tv_id = Auth::user()->tv_id;
            }
        });
    }

    public function tv()
    {
        return $this->belongsTo('App\TV');
    }
}
