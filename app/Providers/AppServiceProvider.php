<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Request;
use Response;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // MariaDB constraint length workaround
        Schema::defaultStringLength(191);

        // Byte streaming with seeking support
        // https://gist.github.com/vluzrmos/d849d67cfd9f44e5f0b6f52e2a374c2e
        Response::macro('streamed', function($type, $size, $name, $callback) {
            $start = 0;
            $length = $size;
            $status = 200;
            $headers = [
                'Content-Type' => $type,
                'Content-Length' => $size,
                'Accept-Ranges' => 'bytes'
            ];
            if (false !== $range = Request::server('HTTP_RANGE', false)) {
                list($param, $range) = explode('=', $range);
                if (strtolower(trim($param)) !== 'bytes') {
                    header('HTTP/1.1 400 Invalid Request');
                    exit;
                }
                list($from, $to) = explode('-', $range);
                if ($from === '') {
                    $end = $size - 1;
                    $start = $end - intval($from);
                } elseif ($to === '') {
                    $start = intval($from);
                    $end = $size - 1;
                } else {
                    $start = intval($from);
                    $end = intval($to);
                }
                if ($end >= $length) {
                    $end = $length - 1;
                }
                $length = $end - $start + 1;
                $status = 206;
                $headers['Content-Range'] = sprintf('bytes %d-%d/%d', $start, $end, $size);
                $headers['Content-Length'] = $length;
            }
            return Response::stream(function() use ($start, $length, $callback) {
                call_user_func($callback, $start, $length);
            }, $status, $headers);
        });
    }
}
