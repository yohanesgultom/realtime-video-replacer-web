<?php

namespace App;

use Eloquent as Model;
use Auth;
use Illuminate\Database\Eloquent\Builder;

class TV extends Model
{

    public $table = 'tvs';

    public $fillable = [
        'name',
        'rtmp_source',
        'logo_mask',
        'logo_masked',
        'logo_x',
        'logo_y',
    ];


    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'rtmp_source' => 'url',
        'logo_mask' => 'image',
        'logo_masked' => 'image',
        'logo_x' => 'integer',
        'logo_y' => 'integer',
    ];

    public function programs()
    {
        return $this->hasMany('App\Program');
    }

    public function users()
    {
        return $this->hasMany('App\User');
    }

    protected static function boot()
    {
        parent::boot();
        
        static::addGlobalScope('owned', function (Builder $builder) {
            if (Auth::check() && !empty(Auth::user()->tv_id)) {
                $builder->where('tvs.id', Auth::user()->tv_id);
            }
        });
    }
}
