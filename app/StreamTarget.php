<?php

namespace App;

use Eloquent as Model;
use Auth;
use Illuminate\Database\Eloquent\Builder;

class StreamTarget extends Model
{

    public $table = 'stream_targets';

    public $fillable = [
        'rtmp_target',
        'start',
        'end',
        'ffmpeg_args_0',
        'ffmpeg_args_1',
        'options',
        'program_id',
        'ad_id',
        'media_stream_id'
    ];

    protected $dates = [
        'start',
        'end',
    ];

    protected $with = [
        'ad',
        'program',
        'media_stream',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'rtmp_target' => 'required|url',
        'program_id' => 'required',
        'ad_id' => 'required',
        'ffmpeg_args_0' => 'regex:/.+{ad_file}.+{rtmp_target}/',
        'ffmpeg_args_1' => 'regex:/.+{rtmp_source}.+{rtmp_target}/',
        'options' => 'json',
        'start' => 'date',
        'end' => 'date|after:start',
    ];

    public function program()
    {
        return $this->belongsTo('App\Program');
    }

    public function media_stream()
    {
        return $this->belongsTo('App\MediaStream');
    }

    public function ad()
    {
        return $this->belongsTo('App\Ad');
    }

    protected static function boot()
    {
        parent::boot();
        
        static::addGlobalScope('owned', function (Builder $builder) {
            if (Auth::check() && !empty(Auth::user()->tv_id)) {
                $builder
                    ->select('stream_targets.*')
                    ->join('programs', 'stream_targets.program_id', 'programs.id')
                    ->where('programs.tv_id', Auth::user()->tv_id);
            }
        });
    }
}
