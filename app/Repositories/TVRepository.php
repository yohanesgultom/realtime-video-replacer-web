<?php

namespace App\Repositories;

use App\TV;
use App\Repositories\BaseRepository;

/**
 * Class TVRepository
 * @package App\Repositories
 * @version April 30, 2019, 5:02 am UTC
*/

class TVRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return TV::class;
    }
}
