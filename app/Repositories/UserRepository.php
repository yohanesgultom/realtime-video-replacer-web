<?php

namespace App\Repositories;

use Auth;
use App\User;
use App\Repositories\BaseRepository;

/**
 * Class UserRepository
 * @package App\Repositories
 * @version May 1, 2019, 9:35 am WIB
*/

class UserRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'role',
        'email',
        'email_verified_at',
        'password',
        'remember_token'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return User::class;
    }

    public function allQuery($search = [], $skip = null, $limit = null)
    {
        $query = $this->model->owned();

        if (count($search)) {
            foreach($search as $key => $value) {
                if (in_array($key, $this->getFieldsSearchable())) {
                    $query->where($key, $value);
                }
            }
        }

        if (!is_null($skip)) {
            $query->skip($skip);
        }

        if (!is_null($limit)) {
            $query->limit($limit);
        }

        return $query;
    }
    
    public function find($id, $columns = ['*'])
    {
        $query = $this->model->owned();
        return $query->find($id, $columns);
    }    

    public function create($input)
    {
        // force to user own tv id
        if (Auth::check() && !empty(Auth::user()->tv_id)) {
            $input['tv_id'] = Auth::user()->tv_id;
        }
        $model = $this->model->newInstance($input);
        $model->save();
        return $model;
    }

    public function update($input, $id)
    {
        // force to user own tv id
        if (Auth::check() && !empty(Auth::user()->tv_id)) {
            $input['tv_id'] = Auth::user()->tv_id;
        }
        $query = $this->model->owned();
        $model = $query->findOrFail($id);
        $model->fill($input);                
        $model->save();
        return $model;
    }

    public function delete($id)
    {
        $query = $this->model->owned();
        $model = $query->findOrFail($id);
        return $model->delete();
    }
}
