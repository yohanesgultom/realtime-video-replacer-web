<?php

namespace App\Repositories;

use App\StreamTarget;
use App\Repositories\BaseRepository;

/**
 * Class StreamTargetRepository
 * @package App\Repositories
 * @version April 30, 2019, 1:45 pm UTC
*/

class StreamTargetRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'rtmp_source',
        'rtmp_target',
        'start',
        'end',
        'program_id',
        'ad_id',
        'media_stream_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return StreamTarget::class;
    }
}
