<?php

namespace App\Repositories;

use App\Ad;
use App\Repositories\BaseRepository;

/**
 * Class AdRepository
 * @package App\Repositories
 * @version April 30, 2019, 12:35 pm UTC
*/

class AdRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'file'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Ad::class;
    }
}
