<?php

namespace App\Repositories;

use App\MediaStream;
use App\Repositories\BaseRepository;

/**
 * Class MediaStreamRepository
 * @package App\Repositories
 * @version April 30, 2019, 1:17 pm UTC
*/

class MediaStreamRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return MediaStream::class;
    }
}
