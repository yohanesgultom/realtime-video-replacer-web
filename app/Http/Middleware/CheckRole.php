<?php

namespace App\Http\Middleware;

use Closure;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, ... $roles)
    {
        foreach($roles as $role) {
            if ($request->user()->role_name == strtoupper($role)) {
                return $next($request);
            }
        }

        $error = "Only {$role} allowed";
        if($request->wantsJson()) {                
            return response()->json([ 'error' => $error ], 401);
        } else {
            abort(401, $error);
        }                
    }
}
