<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\CreateStreamTargetRequest;
use App\Http\Requests\UpdateStreamTargetRequest;
use App\Repositories\StreamTargetRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;
use Auth;
use App\MediaStream;
use App\Program;
use App\Ad;

class StreamTargetController extends AppBaseController
{
    /** @var  StreamTargetRepository */
    private $streamTargetRepository;

    public function __construct(StreamTargetRepository $streamTargetRepo)
    {
        $this->streamTargetRepository = $streamTargetRepo;
    }

    /**
     * Validate ownership of referenced models
     */
    public function validateReference($input)
    {
        if (Auth::check() && !empty(Auth::user()->tv_id)) {
            if (array_key_exists('program_id', $input)) {
                $count = Program::where('programs.tv_id', Auth::user()->tv_id)
                    ->where('programs.id', $input['program_id'])
                    ->count();
                if ($count <= 0) return 'Program not found';
            }
            if (array_key_exists('ad_id', $input)) {
                $count = Ad::where('ads.tv_id', Auth::user()->tv_id)
                    ->where('ads.id', $input['ad_id'])
                    ->count();
                if ($count <= 0) return 'Ad not found';
            }
            if (array_key_exists('media_stream_id', $input)) {
                $count = MediaStream::where('media_streams.tv_id', Auth::user()->tv_id)
                    ->where('media_streams.id', $input['media_stream_id'])
                    ->count();                
                if ($count <= 0) return 'Media Stream not found';
            }            
        }
        return null;
    }

    /**
     * Display a listing of the StreamTarget.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $streamTargets = $this->streamTargetRepository->paginate(10);

        return view('admin.stream_targets.index')
            ->with('streamTargets', $streamTargets);
    }

    /**
     * Show the form for creating a new StreamTarget.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.stream_targets.create');
    }

    /**
     * Store a newly created StreamTarget in storage.
     *
     * @param CreateStreamTargetRequest $request
     *
     * @return Response
     */
    public function store(CreateStreamTargetRequest $request)
    {
        $input = $request->all();
        $error = $this->validateReference($input);
        if (!empty($error)) {
            Flash::error($error);
            return redirect()->back()->withInput();
        }
        $streamTarget = $this->streamTargetRepository->create($input);
        Flash::success('Stream Target saved successfully.');
        return redirect(route('admin.streamTargets.index'));
    }

    /**
     * Display the specified StreamTarget.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $streamTarget = $this->streamTargetRepository->find($id);
        if (empty($streamTarget)) {
            Flash::error('Stream Target not found');
            return redirect(route('admin.streamTargets.index'));
        }
        return view('admin.stream_targets.show')->with('streamTarget', $streamTarget);
    }

    /**
     * Show the form for editing the specified StreamTarget.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $streamTarget = $this->streamTargetRepository->find($id);
        if (empty($streamTarget)) {
            Flash::error('Stream Target not found');
            return redirect(route('admin.streamTargets.index'));
        }
        return view('admin.stream_targets.edit')->with('streamTarget', $streamTarget);
    }

    /**
     * Update the specified StreamTarget in storage.
     *
     * @param int $id
     * @param UpdateStreamTargetRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateStreamTargetRequest $request)
    {
        $streamTarget = $this->streamTargetRepository->find($id);
        if (empty($streamTarget)) {
            Flash::error('Stream Target not found');
            return redirect(route('admin.streamTargets.index'));
        }

        $input = $request->all();
        $error = $this->validateReference($input);
        if (!empty($error)) {
            Flash::error($error);
            return redirect()->back()->withInput();
        }
        if ($request->hasFile('logo_mask')) {
            $logo_mask_file = $request->file('logo_mask')->getClientOriginalName();
            $input['logo_mask'] = $request->file('logo_mask')->storeAs("stream_targets/{$streamTarget->id}/logo_mask", $logo_mask_file, 'local');
        }
        if ($request->hasFile('logo_masked')) {
            $logo_masked_file = $request->file('logo_masked')->getClientOriginalName();
            $input['logo_masked'] = $request->file('logo_masked')->storeAs("stream_targets/{$streamTarget->id}/logo_masked", $logo_masked_file, 'local');
        }
        $streamTarget = $this->streamTargetRepository->update($input, $id);

        Flash::success('Stream Target updated successfully.');
        return redirect(route('admin.streamTargets.index'));
    }

    /**
     * Remove the specified StreamTarget from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $streamTarget = $this->streamTargetRepository->find($id);

        if (empty($streamTarget)) {
            Flash::error('Stream Target not found');

            return redirect(route('admin.streamTargets.index'));
        }

        $this->streamTargetRepository->delete($id);

        Flash::success('Stream Target deleted successfully.');

        return redirect(route('admin.streamTargets.index'));
    }

}
