<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\CreateMediaStreamRequest;
use App\Http\Requests\UpdateMediaStreamRequest;
use App\Repositories\MediaStreamRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class MediaStreamController extends AppBaseController
{
    /** @var  MediaStreamRepository */
    private $mediaStreamRepository;

    public function __construct(MediaStreamRepository $mediaStreamRepo)
    {
        $this->mediaStreamRepository = $mediaStreamRepo;
    }

    /**
     * Display a listing of the MediaStream.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $mediaStreams = $this->mediaStreamRepository->allQuery()->with('tv')->paginate(10);

        return view('admin.media_streams.index')
            ->with('mediaStreams', $mediaStreams);
    }

    /**
     * Show the form for creating a new MediaStream.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.media_streams.create');
    }

    /**
     * Store a newly created MediaStream in storage.
     *
     * @param CreateMediaStreamRequest $request
     *
     * @return Response
     */
    public function store(CreateMediaStreamRequest $request)
    {
        $input = $request->all();

        $mediaStream = $this->mediaStreamRepository->create($input);

        Flash::success('Media Stream saved successfully.');

        return redirect(route('admin.mediaStreams.index'));
    }

    /**
     * Display the specified MediaStream.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $mediaStream = $this->mediaStreamRepository->find($id);

        if (empty($mediaStream)) {
            Flash::error('Media Stream not found');

            return redirect(route('admin.mediaStreams.index'));
        }

        return view('admin.media_streams.show')->with('mediaStream', $mediaStream);
    }

    /**
     * Show the form for editing the specified MediaStream.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $mediaStream = $this->mediaStreamRepository->find($id);

        if (empty($mediaStream)) {
            Flash::error('Media Stream not found');

            return redirect(route('admin.mediaStreams.index'));
        }

        return view('admin.media_streams.edit')->with('mediaStream', $mediaStream);
    }

    /**
     * Update the specified MediaStream in storage.
     *
     * @param int $id
     * @param UpdateMediaStreamRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateMediaStreamRequest $request)
    {
        $mediaStream = $this->mediaStreamRepository->find($id);

        if (empty($mediaStream)) {
            Flash::error('Media Stream not found');

            return redirect(route('admin.mediaStreams.index'));
        }

        $mediaStream = $this->mediaStreamRepository->update($request->all(), $id);

        Flash::success('Media Stream updated successfully.');

        return redirect(route('admin.mediaStreams.index'));
    }

    /**
     * Remove the specified MediaStream from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $mediaStream = $this->mediaStreamRepository->find($id);

        if (empty($mediaStream)) {
            Flash::error('Media Stream not found');

            return redirect(route('admin.mediaStreams.index'));
        }

        $this->mediaStreamRepository->delete($id);

        Flash::success('Media Stream deleted successfully.');

        return redirect(route('admin.mediaStreams.index'));
    }
}
