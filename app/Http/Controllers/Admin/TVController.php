<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\CreateTVRequest;
use App\Http\Requests\UpdateTVRequest;
use App\Repositories\TVRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use Flash;
use Response;

class TVController extends AppBaseController
{
    /** @var  TVRepository */
    private $tVRepository;

    public function __construct(TVRepository $tVRepo)
    {
        $this->tVRepository = $tVRepo;
    }

    /**
     * Display a listing of the TV.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $tVs = $this->tVRepository->paginate(10);

        return view('admin.tvs.index')
            ->with('tVs', $tVs);
    }

    /**
     * Show the form for creating a new TV.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.tvs.create');
    }

    /**
     * Store a newly created TV in storage.
     *
     * @param CreateTVRequest $request
     *
     * @return Response
     */
    public function store(CreateTVRequest $request)
    {
        $input = $request->all();
        $tV = $this->tVRepository->create($input);
        $has_file = false;
        if ($request->hasFile('logo_mask')) {
            $logo_mask_file = $request->file('logo_mask')->getClientOriginalName();
            $tV->logo_mask = $request->file('logo_mask')->storeAs("tvs/{$tV->id}/logo_mask", $logo_mask_file, 'local');
            $has_file = true;
        }
        if ($request->hasFile('logo_masked')) {
            $logo_masked_file = $request->file('logo_masked')->getClientOriginalName();
            $tV->logo_masked = $request->file('logo_masked')->storeAs("tvs/{$tV->id}/logo_masked", $logo_masked_file, 'local');
        }
        if ($has_file) $tV->save();
        Flash::success('TV saved successfully.');
        return redirect(route('admin.tvs.index'));
    }

    /**
     * Display the specified TV.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $tV = $this->tVRepository->find($id);

        if (empty($tV)) {
            Flash::error('TV not found');

            return redirect(route('admin.tvs.index'));
        }

        return view('admin.tvs.show')->with('tV', $tV);
    }

    /**
     * Show the form for editing the specified TV.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $tV = $this->tVRepository->find($id);

        if (empty($tV)) {
            Flash::error('TV not found');

            return redirect(route('admin.tvs.index'));
        }

        return view('admin.tvs.edit')->with('tV', $tV);
    }

    /**
     * Update the specified TV in storage.
     *
     * @param int $id
     * @param UpdateTVRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTVRequest $request)
    {
        $tV = $this->tVRepository->find($id);
        if (empty($tV)) {
            Flash::error('TV not found');
            return redirect(route('admin.tvs.index'));
        }

        $input = $request->all();
        if ($request->hasFile('logo_mask')) {
            $logo_mask_file = $request->file('logo_mask')->getClientOriginalName();
            $input['logo_mask'] = $request->file('logo_mask')->storeAs("tvs/{$tV->id}/logo_mask", $logo_mask_file, 'local');
        }
        if ($request->hasFile('logo_masked')) {
            $logo_masked_file = $request->file('logo_masked')->getClientOriginalName();
            $input['logo_masked'] = $request->file('logo_masked')->storeAs("tvs/{$tV->id}/logo_masked", $logo_masked_file, 'local');
        }
        $tV = $this->tVRepository->update($input, $id);
        Flash::success('TV updated successfully.');
        return redirect(route('admin.tvs.index'));
    }

    /**
     * Remove the specified TV from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $tV = $this->tVRepository->find($id);
        if (empty($tV)) {
            Flash::error('TV not found');
            return redirect(route('admin.tvs.index'));
        }
        $this->tVRepository->delete($id);
        Flash::success('TV deleted successfully.');
        return redirect(route('admin.tvs.index'));
    }

    public function download(Request $req, $id)
    {   
        $tV = $this->tVRepository->find($id);
        if (empty($tV)) {
            return response(null, 404);
        }
        $file_type = $req->input('file', 'logo_mask');
        return Storage::disk('local')->download($tV->$file_type);
    }
}
