<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\CreateAdRequest;
use App\Http\Requests\UpdateAdRequest;
use App\Repositories\AdRepository;
use App\Http\Controllers\AppBaseController;
use Storage;
use Request;
use Flash;
use Response;

class AdController extends AppBaseController
{
    /** @var  AdRepository */
    private $adRepository;

    public function __construct(AdRepository $adRepo)
    {
        $this->adRepository = $adRepo;
    }

    /**
     * Display a listing of the Ad.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $ads = $this->adRepository->allQuery()->with('tv')->paginate(10);

        return view('admin.ads.index')
            ->with('ads', $ads);
    }

    /**
     * Show the form for creating a new Ad.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.ads.create');
    }

    /**
     * Store a newly created Ad in storage.
     *
     * @param CreateAdRequest $request
     *
     * @return Response
     */
    public function store(CreateAdRequest $request)
    {
        $input = $request->all();
        $ad = $this->adRepository->create($input);
        if ($request->hasFile('file')) {
            $filename = $request->file('file')->getClientOriginalName();
            $ad->file = $request->file('file')->storeAs("ads/{$ad->id}/file", $filename, 'local');
            $ad->save();
        }
        Flash::success('Ad saved successfully.');
        return redirect(route('admin.ads.index'));
    }

    /**
     * Display the specified Ad.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ad = $this->adRepository->find($id);

        if (empty($ad)) {
            Flash::error('Ad not found');

            return redirect(route('admin.ads.index'));
        }

        return view('admin.ads.show')->with('ad', $ad);
    }

    /**
     * Show the form for editing the specified Ad.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ad = $this->adRepository->find($id);

        if (empty($ad)) {
            Flash::error('Ad not found');

            return redirect(route('admin.ads.index'));
        }

        return view('admin.ads.edit')->with('ad', $ad);
    }

    /**
     * Update the specified Ad in storage.
     *
     * @param int $id
     * @param UpdateAdRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateAdRequest $request)
    {
        $ad = $this->adRepository->find($id);
        if (empty($ad)) {
            Flash::error('Ad not found');
            return redirect(route('admin.ads.index'));
        }

        $input = $request->all();
        if ($request->hasFile('file')) {
            $filename = $request->file('file')->getClientOriginalName();
            $input['file'] = $request->file('file')->storeAs("ads/{$id}/file", $filename, 'local');
        }
        $ad = $this->adRepository->update($input, $id);

        Flash::success('Ad updated successfully.');

        return redirect(route('admin.ads.index'));
    }

    /**
     * Remove the specified Ad from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ad = $this->adRepository->find($id);

        if (empty($ad)) {
            Flash::error('Ad not found');

            return redirect(route('admin.ads.index'));
        }

        $this->adRepository->delete($id);

        Flash::success('Ad deleted successfully.');

        return redirect(route('admin.ads.index'));
    }

    public function download($id)
    {   
        $ad = $this->adRepository->find($id);
        if (empty($ad)) {
            return response(null, 404);
        }
        $size = Storage::size($ad->file);
        $type = Storage::mimeType($ad->file);
        $path = Storage::disk('local')->path($ad->file);
        $name = basename($path);
        return Response::streamed($type, $size, $name, function($offset, $length) use ($path) {
            $stream = \GuzzleHttp\Psr7\stream_for(fopen($path, 'r'));
            $stream->seek($offset);
            while (!$stream->eof()) {
                echo $stream->read($length);
            }
            $stream->close();
        });
    }
}
