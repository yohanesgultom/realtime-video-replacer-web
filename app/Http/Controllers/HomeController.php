<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;

class HomeController extends Controller
{
    public function index()
    {
        if (Auth::user()->role == User::ROLES['ADMIN']) {
            return view('admin.home');
        } else {
            // TODO:
            return view('admin.home');
        }        
    }
}
